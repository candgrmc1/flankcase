<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = [
              [
                  "name" => "Kategori 1"
              ],
              [
                  "name" => "Kategori 2",
                  "parent_id" => 1
              ],
            [
                "name" => "Kategori 3",
                "parent_id" => 1
            ],
            [
                "name" => "Kategori 4",
                "parent_id" => 2
            ],
            [
                "name" => "Kategori 5",
                "parent_id" => 3
            ],
            [
                "name" => "Kategori 6"
            ],
            [
                "name" => "Kategori 7",
                "parent_id" => 4
            ],
            [
                "name" => "Kategori 8",
                "parent_id" => 1
            ],
            [
                "name" => "Kategori 9",
                "parent_id" => 1
            ],
        ];

        foreach ($categories as $c){
            Category::create($c);
        }

    }
}
