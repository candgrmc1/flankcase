<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                "name" => "Ürün 1",
                "category_id" => 1
            ],
            [
                "name" => "Ürün 2",
                "category_id" => 1
            ],
            [
                "name" => "Ürün 3",
                "category_id" => 2
            ],
            [
                "name" => "Ürün 4",
                "category_id" => 2
            ],
            [
                "name" => "Ürün 5",
                "category_id" => 5
            ],
            [
                "name" => "Ürün 6",
                "category_id" => 1
            ],
            [
                "name" => "Ürün 7",
                "category_id" => 1
            ],
            [
                "name" => "Ürün 8",
                "category_id" => 1
            ],
            [
                "name" => "Ürün 9",
                "category_id" => 1
            ],
            [
                "name" => "Ürün 10",
                "category_id" => 7
            ]
        ];

        foreach ($products as $p){
            Product::create($p);
        }
    }
}
