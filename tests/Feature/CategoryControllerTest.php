<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateCategories()
    {
        $response = $this->json('POST','/api/categories', ["name" => "testCase", "parent_id" => 1]);
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);

    }

    public function testGetCategories(){
        $response = $this->get('/api/categories');
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);
    }

    public function testUpdateCategory()
    {
        $response = $this->json('PUT','/api/categories', ["id" => 3,"name" => "testCase-updated"]);
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);

    }
    public function testDeleteCategory()
    {
        $response = $this->json('DELETE','/api/categories', ["id" => 9]);
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);

    }
}
