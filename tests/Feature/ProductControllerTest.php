<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateProduct()
    {
        $response = $this->json('POST','/api/products', ["name" => "testCase", "category_id" => 2]);
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);

    }

    public function testGetProducts(){
        $response = $this->get('/api/products');
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);
    }
    public function testGetProductsByCategory(){
        $response = $this->get('/api/products-by-category/1');
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);
    }


    public function testDeleteProduct()
    {
        $response = $this->json('DELETE','/api/products', ["id" => 9]);

        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);

    }
    public function testUpdateProduct()
    {
        $response = $this->json('PUT','/api/products', ["id" => 3,"name" => "testCase-updated", "category_id" => 2]);
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);

    }
}
