<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class ProductControllerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {

        $response = $this->json('POST','/api/products', ["name" => "testCase", "category_id" => 2]);
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);
        print 'product created';
        $response = $this->get('/api/products');
        $response->assertStatus(200)->assertJson([
            'success' => 'ok',
        ]);
    }
}
