<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
  NO AUTH
 */

/**Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::get('/',function(Request $request) {
    return  response()->json(['hello'=> 'world']);
});

/** Category */
Route::get('/categories','CategoryController@get')->name('categories.get');
Route::post('/categories','CategoryController@create');
Route::put('/categories','CategoryController@update');
Route::delete('/categories','CategoryController@delete');

/** Products */
Route::get('/products','ProductController@get')->name('products.get');
Route::get('/products-by-category/{id}','ProductController@getByCategoryId');
Route::post('/products','ProductController@create');
Route::put('/products','ProductController@update');
Route::delete('/products','ProductController@delete');
