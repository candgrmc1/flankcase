<?php


namespace App\Transformers;

use App\Product;

class ProductTransformer
{
    public function transform(Product $product){
        return [
          'id' => $product->id,
          'name' => $product->name,
          'category' => $product->category
        ];
    }

    public function transformMany($products){
        return array_map(fn($product) => [
            'id' => $product['id'],
            'name' => $product['name'],
            'category' => $product['category']
        ], $products);
    }


}
