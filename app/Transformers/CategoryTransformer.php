<?php


namespace App\Transformers;

use App\Category;

class CategoryTransformer
{
    public function transform(Category $category){
        return [
          'id' => $category->id,
          'name' => $category->name,
          'parent_id' => $category->parent_id
        ];
    }

    public function transformMany($categories){
        return array_map(fn($category) => [
            'id' => $category['id'],
            'name' => $category['name'],
            'parent_id' => $category['parent_id'],
            'children' => count($category['nested']) ? $this->transformMany($category['nested']) : []
        ], $categories);
    }


}
