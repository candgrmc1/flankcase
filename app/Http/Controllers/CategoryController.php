<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Transformers\CategoryTransformer;
use Validator;

class CategoryController extends Controller
{
    public function get(){
        try{
            $response = (new CategoryTransformer)
                ->transformMany(Category::with('nested')
                    ->whereNull('parent_id')->get()->toArray());
            return $this->success($response);
        }catch (\Exception $e){
            return $this->error($e->getMessage());
        }
    }

    public function create(Request $request){
        $data = $request->toArray();
        $rules = [
            'name' => 'string|required',
            'parent_id' => 'integer'
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {
            // ok
        } else {
            return $this->validationError($validator->errors()->all());
        }

        Category::create($request->only(['name','parent_id']));
        return $this->created();
    }

    public function update(Request $request){
        $data = $request->toArray();
        $rules = [
            'id' => 'integer|required',
            'name' => 'string',
            'parent_id' => 'integer'
        ];
        $validator = Validator::make($data, $rules);

        if ($validator->passes()) {
            // ok
        } else {
            return $this->validationError($validator->errors()->all());
        }

        try{
            $category = Category::find($request->id);
            if($category){
                // ok
            }else{
                return $this->error('Category not found');
            }
            $category->update($request->only(['name','parent_id']));
            return $this->updated();
        }catch (\Exception $e){
            return $this->error($e->getMessage());
        }

    }

    public function delete(Request $request){
        $data = $request->toArray();
        $rules = [
            'id' => 'integer|required'
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {
            // ok
        } else {
            return $this->validationError($validator->errors()->all());
        }

        try{
            $category = Category::find($request->id);
            if($category){
                // ok
            }else{
                return $this->error('Category not found');
            }
            $category->delete();
            Category::where('parent_id',$request->id)->delete();

            return $this->deleted();

        }catch (\Exception $e){
            return $this->error($e->getMessage());
        }

    }
}
