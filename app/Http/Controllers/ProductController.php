<?php

namespace App\Http\Controllers;

use App\Http\Helper;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\Validator;
use App\Transformers\ProductTransformer;
class ProductController extends Controller
{
    //
    public function __construct()
    {
        $this->helper = new Helper();
    }

    public function get(){
        $response = (new ProductTransformer)
            ->transformMany(Product::with('category')
                ->get()->toArray());
        return $this->success($response);
    }

    public function getByCategoryId(Request $request){
        $category = Category::find($request->id);

        if($category){
            //ok
        }else{
            return $this
                ->error('Category not found');
        }

        $childrenIds = $category->getCategoriesAssoc()->pluck('id')->toArray();
        $ids = [$category->id,...$childrenIds];
        $products = Product::whereIn('category_id',$ids)->get();

        return $this
                ->success($products);

    }

    public function create(Request $request){
        $data = $request->toArray();
        $rules = [
            'name' => 'string|required',
            'category_id' => 'integer|| required'
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {
            // ok
        } else {
            return $this->validationError($validator
                ->errors()
                ->all());

        }
        try{
            Product::create($request->only(['name','category_id']));
            return $this->created();
        }
        catch (\Exception $e){
            return $this->error($e
                ->getMessage());
        }

    }

    public function update(Request $request){
        $data = $request->toArray();
        $rules = [
            'id' => 'integer|required',
            'name' => 'string',
            'category_id' => 'integer'
        ];
        $validator = Validator::make($data, $rules);

        if ($validator->passes()) {
            // ok
        } else {
            return $this->validationError($validator
                ->errors()
                ->all());

        }
        try{
            $product = Product::find($request->id);
            if($product){
                //ok
            }else{
                return $this->error('Product not found');
            }
            $product->update($request->only(['name','parent_id']));

            return $this->updated();
        }
        catch (\Exception $e){

            return $this->error($e->getMessage());
        }




    }

    public function delete(Request $request){
        $data = $request->toArray();
        $rules = [
            'id' => 'integer|required'
        ];
        $validator = Validator::make($data, $rules);

        if ($validator->passes()) {
            // ok
        } else {
        return $this->validationError($validator
            ->errors()
            ->all());
        }
        try{
            Product::find($request->id)->delete();
            return $this->deleted();

        }
        catch (\Exception $e){
            return $this->error($e
                ->getMessage());
        }



    }
}
