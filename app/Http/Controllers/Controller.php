<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($data){

        return response()->json([
            'success' => 'ok',
            'data' => $data
        ],200);
    }

    public function error($message){
        return response()
            ->json([
            'success' => 'error',
            'message' => $message
        ],500);
    }

    public function validationError($arr){
        return response()
            ->json([
            'success' => 'validation-error',
            'errors' => $arr
        ]);
    }

    public function created(){
        return response()
            ->json([
            'success' => 'ok',
            'message' => 'created'
        ],200);
    }
    public function updated(){
        return response()
            ->json([
                'success' => 'ok',
                'message' => 'updated'
            ]);
    }
    public function deleted(){
        return response()
            ->json([
            'success' => 'ok',
            'message' => 'deleted'
        ],200);
    }
}
