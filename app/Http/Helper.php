<?php


namespace App\Http;


class Helper
{
    public function do_if($cond, $func){
        if($cond){
            return $func();
        }
    }
}
