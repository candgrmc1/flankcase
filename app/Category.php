<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Collection;
class Category extends Model
{
    //
    use LogsActivity;
    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;


    protected $fillable = ['name','parent_id'];

    public function parent(){

        return $this->belongsTo('App\Category', 'parent_id');
    }
    public function children(){
        return $this->hasMany('App\Category', 'parent_id');
    }
    public function products(){
        return $this->hasMany('App\Product');
    }
    public function nested()
    {
        return $this->children()->with(['nested']);
    }
    public function getCategoriesAssoc ()
    {
        $sections = new Collection();
        foreach ($this->children as $section) {
            $sections->push($section);
            $sections = $sections->merge($section->getCategoriesAssoc());
        }
        return $sections;
    }


}
